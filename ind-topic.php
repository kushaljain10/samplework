<?php
session_start();
$alert=0;
if(!isset($_SESSION['name'])){
    $_SESSION['login']=1;
    echo "<script>window.location.href = 'index.php'</script>";
}
else{
    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
    $profile_pic = $_SESSION['profile_pic'];
    $_SESSION['login']=2;
}

require_once('dbconfig.php');

if(isset($_POST['submit'])){
    if($_POST['topic']=='' || ctype_space($_POST['topic'])){
        $alert=1;
    }
    else{
        $topic = addslashes($_POST['topic']);
        $sql = "INSERT INTO individual_topic VALUES('$name', '$topic')";
        $conn->query($sql);
        
        $conn->close();
        
        $alert=2;
    }   
}
?>

<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="bootstrap.css">
        <link rel="stylesheet" href="style.css">
        <link rel="icon" href="logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Individual Topics</title>
    </head>

    <body>

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="main.php">
                <img src="logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                Social Beat Tech Team Dashboard
            </a>
            <div class="dropdown">
            <div href="#" id="nav-menu" class="dropbtn">
                <?php echo $name; ?>
                <img src="<?php echo 'images\\'.$profile_pic ?>" id="profile">
                <div class="dropdown-content">
                <a href="profile.php">My Profile</a>
                <a href="logout.php">Log out</a>
              </div>
            </div>
            </div>
        </nav>

        
        <form method="post">
          <div class="form-group">
            <label for="name">Name</label>
            <input type="text" id="name" value="<?php echo $name;?>" class="form-control" name="name" readonly>
          </div>
          <div class="form-group">
            <label for="topic">Topic</label>
            <input type="text" class="form-control" name="topic" id="topic" placeholder="Enter Topic" required>
          </div>
            <div class="form-group"><input class="btn btn-primary" type="submit" name="submit" value="Submit details" />
            <input  class="btn btn-secondary" type="reset" value="Reset" id="reset" /></div>
        </form>
        
<!--
        <form method="post">
        
            <center>
            <div id="div1"><label for="name">Name: </label><input type="text" id="name" value="" class="form-control" name="name" readonly></div>
            <div><label for="topic">Topic: </label><input type="text" id="topic" name="topic" placeholder="Enter topic..."></div>
            <div><input class="btn btn-primary" type="submit" name="submit" value="Submit details!" />
            <input  class="btn btn-secondary" type="reset" value="Reset" id="reset" /></div>
            </center>
            
        </form>
-->
        <?php
        
            switch($alert){
                case 1:  echo "<div id='alert'>Please enter a valid input.</div>";
                    break;
                case 2: header('Location: ind-topic.php');
                    break;
            }
            
        ?>
        <table class="table table-striped table-bordered table-responsive-md">
        
            <?php
            
            $sql = "SELECT * from individual_topic";
            $result = $conn->query($sql);
            
            if ($result->num_rows > 0){
                echo "<br><b><center>Existing topics:<br><br></center></b>";
                echo "<thead class='thead-dark'><tr><th>Name: Topic</th></tr></thead>";
                
                while($row = $result->fetch_assoc()){
                    echo "<tr><td><b>".$row['name'].":</b> ".$row['topic']."</td></tr>";
                }
            }
            else{
                echo "<br><br><b><center>Existing topics:<br>";
                echo "<br><br>No Data Found</center></b>";
            }
            
            $conn->close();
        
            ?>
        </table>
        <script>
            document.getElementById('reset').addEventListener("click", function(){window.location.href = window.location.href;});
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
        </script>
    </body>
</html>