    <?php
    session_start();
    $alert=0;
    if(isset($_SESSION['name'])){
        $_SESSION['already'] = true;
        echo "<script>window.location.href = 'main.php';</script>";
    }

    if(isset($_SESSION['login'])){
        if ($_SESSION['login']==0){
            session_destroy();    
            session_start();
            $_SESSION['login']=0;
        }
        else if($_SESSION['login']==1){  
            $alert=1;
            $_SESSION['login']=0;
        }
    }

    date_default_timezone_set('Asia/Kolkata');
    $date = date('Y-m-d H:i:s');

    require_once('dbconfig.php');

    if (!$conn) {
       die("Connection failed: " . mysqli_connect_error());
    }

    if(isset($_POST['login-button'])){
        $email = $_POST['email'];
        $pass = $_POST['password'];

        $sql = "SELECT ID, name, email, password, profile_pic FROM users where email='$email'";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            if($email == $row['email'] && $pass == $row['password']){
                $name = $row['name'];
                $_SESSION['error']=0;
                $_SESSION['email'] = $email;
                $_SESSION['name'] = $name;
                $_SESSION['password'] = $pass;
                $_SESSION['id'] = $row['ID'];
                $_SESSION['profile_pic'] = $row['profile_pic'];

                $sql = "UPDATE users SET lastAccess = CURRENT_TIME() where email = '$email'";
                $conn->query($sql);

                echo "<script>window.location.href='main.php';</script>";
            }
            else{
                    $alert = 2;
            }
        }
        else{
            if(!$_POST['email']){
                $alert=3;
            }
            else{
                $alert=4;
            }
        }
        $conn->close();
    }

                if(isset($_POST['signup-button'])){
                    $name = $_POST['name'];
                    $email = $_POST['email'];
                    $pass = $_POST['password'];
                    $profile_pic = $_SESSION['profile_pic'];

                    $sql = "SELECT * FROM users where email = '$email'";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                        $alert=5;
                       }
                    else{

                        $sql = "SELECT ID FROM users ORDER BY ID DESC LIMIT 1;";

                        $result = $conn->query($sql);
                        
                        $sql = "INSERT INTO users(name, email, password, accountCreated, lastAccess, profile_pic) VALUES('$name', '$email', '$pass', '$date', CURRENT_TIME(), 'no-pic.jpg')";

                        if($conn->query($sql) === TRUE){
                            $alert=6;
                        }
                        else{
                            echo "Error: " . $sql . "<br>" . $conn->error;
                        }

                    }
                    $conn->close();
                }

    ?>
    <!DOCTYPE html>
    <html>

        <head>
            <link rel="stylesheet" href="bootstrap.css">
            <link rel="stylesheet" href="style.css">
            <link rel="icon" href="logo.png">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Tech Team Dashboard</title>
        </head>
        <body>

            <nav class="navbar navbar-dark bg-dark">
                <a class="navbar-brand" href="index.php">
                    <img src="logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                    Social Beat Tech Team Dashboard
                </a>
            </nav>
            <div id="box">

                <div id="login-signup">
                    <button type="button" class="btn btn-dark" id="login-display">Log In</button>
                    <button type="button" class="btn btn-dark" id="signup-display">Sign Up</button>
                </div>

                <form id="login-form" method="post">
                  <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-10">
                      <button type="submit" class="btn btn-primary" id="login-button" name="login-button">Log in</button>
                        <button type="reset" class="btn btn-secondary" id="reset-button" name="reset-button">Reset</button>
                    </div>
                  </div>
                </form>

                <form id="signup-form" method="post">
                    <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <input type="name" name="name" class="form-control" id="name" placeholder="Name" required>
                    </div>
                  </div>
                    <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" name="password" class="form-control" id="password2" placeholder="Password" required>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-10">
                      <button type="submit" class="btn btn-primary" id="signup-button" name="signup-button">Sign up</button>
                        <button type="reset" class="btn btn-secondary" id="reset-button" name="reset-button">Reset</button>
                    </div>
                  </div>
                </form>

            </div>
            <br>

            <?php
            if(isset($alert)){
                switch($alert){
                    case 1: echo "<br><div id='alert'>Please login first!</div>";
                        break;
                    case 2: echo "<br><div id='alert'>Invalid password, try again.</div>";
                        break;
                    case 4: echo "<br><div id='alert'>Invalid email ID, try again.</div>";
                        break;
                    case 5: echo "<br><div id='alert'>Email ID already exists!</div>";
                        break;
                    case 6: echo "<br><div id='alert' style='color: green;'>User created successfully!</div>";
                        break;
                    default: break;
                }
            }
            if($alert==5)
                {
                   echo '<script>document.getElementById("login-form").style.display = "none";
                    document.getElementById("signup-form").style.display = "block";</script>';
                    $alert=7;
                }
            ?>

            <script>

                document.getElementById("password").addEventListener("input", function (){
                    var str = this.value;
                    if (str.search(/'|"/g) !== -1) {
                        alert("\' or \" is not allowed");
                        document.getElementById("password").value = '';
                    }
                }, false);
                document.getElementById("password2").addEventListener("input", function (){
                    var str = this.value;
                    if (str.search(/'|"/g) !== -1) {
                        alert("\' or \" is not allowed");
                        document.getElementById("password2").value = '';
                    }
                }, false);

                document.getElementById("login-display").addEventListener("click", function(){
                    document.getElementById("login-form").style.display = "block";
                    document.getElementById("signup-form").style.display = "none";
                });
                document.getElementById("signup-display").addEventListener("click", function(){
                    document.getElementById("login-form").style.display = "none";
                    document.getElementById("signup-form").style.display = "block";
                });
                if ( window.history.replaceState ) {
                    window.history.replaceState( null, null, window.location.href );
                }

            </script>
        </body>
    </html>