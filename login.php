<?php
session_start();
$alert=0;
if(isset($_SESSION['name'])){
    $_SESSION['already'] = true;
    header('Location: main.php');
}

if(isset($_SESSION['login'])){
    if ($_SESSION['login']==0){
        session_destroy();    
        session_start();
        $_SESSION['login']=0;
    }
    else if($_SESSION['login']==1){  
        $alert=1;
        $_SESSION['login']=0;
    }
}

date_default_timezone_set('Asia/Kolkata');
$date = date('Y-m-d H:i:s');

$servername = "localhost";
$username = "root";
$password = "";
$db = "socialbeattech";

$conn = mysqli_connect($servername, $username, $password,$db);

if (!$conn) {
   die("Connection failed: " . mysqli_connect_error());
}

if(isset($_POST['login-button'])){
    $email = $_POST['email'];
    $pass = $_POST['password'];
    
    $sql = "SELECT name, email, password FROM users where email='$email'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        if($email == $row['email'] && $pass == $row['password']){
            $name = $row['name'];
            $_SESSION['error']=0;
            $_SESSION['email'] = $email;
            $_SESSION['name'] = $name;
            
            $sql = "UPDATE users SET lastAccess = CURRENT_TIME() where email = '$email'";
            $conn->query($sql);
            
            header('Location: main.php');
        }
        else{
                $alert = 2;
        }
    }
    else{
        if(!$_POST['email']){
            $alert=3;
        }
        else{
            $alert=4;
        }
    }
    $conn->close();
}
        
            if(isset($_POST['signup-button'])){
                $name = $_POST['name'];
                $email = $_POST['email'];
                $pass = $_POST['password'];
                
                $sql = "SELECT * FROM users";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {

                   while($row = $result->fetch_assoc()){
                       $currID = $row['ID'];
                       if(strcasecmp($email,$row['email'])==0){
                           $alert=5;
                           exit();
                       }
                   }
                    $ID = $currID;
                    $ID = $ID+1;
                    $sql = "INSERT INTO users VALUES($ID, '$name', '$email', '$pass', '$date', CURRENT_TIME())";
                    
                    if($conn->query($sql) === TRUE){
                        $alert=6;
                    }
                    else{
                        echo "Error: " . $sql . "<br>" . $conn->error;
                    }

                } else {
                    $ID = 1000;
                    $sql = "INSERT INTO users VALUES($ID, '$name', '$email', '$pass', '$date', CURRENT_TIME())";
                    
                    if($conn->query($sql) === TRUE){
                        $alert=6;
                    }
                    else{
                        echo "Error: " . $sql . "<br>" . $conn->error;
                    }
                }

                $conn->close();
        
            }
?>
<!DOCTYPE html>
<html>

    <head>
        <link rel="icon" href="logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tech Team Dashboard</title>
        <style>
            body{margin: 0; font-family: sans-serif;}
            header{
                background-color: #333;
                padding: 10px;
                text-align: center;
                color: #fff;
                font-size: 2em;
                margin-bottom: 40px;
            }

            button {
              background-color: #4CAF50;
              color: white;
              padding: 14px 20px;
              margin: 8px 0;
              border: none;
              cursor: pointer;
              width: 100%;
              opacity: 0.9;
            }
            #login-display:hover, #signup-display:hover{
                background-color: #4CA150;
            }
            #login-signup{
                width: auto;
                max-width: 300px;
                margin: 0 auto;
            }
            #login-signup:after{
                display: block;
                content: "";
                clear: both;
            }
            #login-signup button{
                float: left;
                width: 50%;
            }
            #loginbox{
                box-shadow: -1px 4px 10px #4CAF50;
                width: 80%;
                max-width: 500px;
                padding: 15px;
                margin: -7px auto;
                display: block;
            }
            #loginbuttons{
                width: 90%;
                margin: 0 auto;
            }
            input{
                width: 90%;
                padding: 10px 5px;
                margin: 3% 5%;
            }
            label{
                width: 100%;
                padding: 10px 0;
                margin: 5px 5%;
            }
            #signupbox{
                box-shadow: -1px 4px 10px #4CAF50;
                width: 80%;
                max-width: 500px;
                padding: 15px;
                margin: -23px auto;
                display: none;
            }
            #alert{
                text-align: center;
                color: red;
            }
        </style>
    </head>
    <body>
    
        <header>Tech Team Dashboard</header>
        <div id="box">
        
            <div id="login-signup">
                <button type="button" id="login-display">Log In</button>
                <button type="button" id="signup-display">Sign Up</button>
            </div>
            <form method="POST" action="">
            
                <div id="loginbox">
                    <label for="email">Email ID: </label>
                    <input type="email" placeholder="Email" name="email" id="email">
                    <label for="password">Password: </label>
                    <input type="password" placeholder="Password" name="password" id="password">
                    <div id="loginbuttons">
                        <button type="submit" id="login-button" name="login-button">Login</button>
                        <button type="reset" id="reset-button" name="reset-button">Reset</button>
                    </div>
                </div>
                
            </form>
            <form method="POST">
            
                <div id="signupbox">
                    <label for="name">Name: </label>
                    <input type="text" placeholder="Name" name="name" id="name">
                    <label for="email">Email ID: </label>
                    <input type="email" placeholder="Email" name="email" id="email">
                    <label for="password">Password: </label>
                    <input type="password" placeholder="Password" name="password" id="password">
                    <button type="submit" id="signup-button" name="signup-button">Signup</button>
                    <button type="reset" id="reset-button">Reset</button>
                </div>
                
            </form>
        
        </div>
        <br>
        
        <?php
        if(isset($alert)){
            switch($alert){
                case 1: echo "<br><div id='alert'>Please login first!</div>";
                    break;
                case 2: echo "<br><div id='alert'>Invalid password, try again.</div>";
                    break;
                case 3: echo "<br><div id='alert'>Please enter an email ID.</div>";
                    break;
                case 4: echo "<br><div id='alert'>Invalid email ID, try again.</div>";
                    break;
                case 5: echo "<br><div id='alert'>Email ID already exists!</div>";
                    break;
                case 6: echo "<br><div id='alert' style='color: green;'>User created successfully!</div>";
                    break;
                default: break;
            }
        }
        ?>
        
        <script>
        
            document.getElementById("login-display").addEventListener("click", function(){
                document.getElementById("loginbox").style.display = "block";
                document.getElementById("signupbox").style.display = "none";
            });
            document.getElementById("signup-display").addEventListener("click", function(){
                document.getElementById("loginbox").style.display = "none";
                document.getElementById("signupbox").style.display = "block";
            });
        
        </script>
    </body>
</html>