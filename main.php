<?php
session_start();

$alert=0;

if(!isset($_SESSION['name'])){
    $_SESSION['login']=1;
    echo "<script>window.location.href = 'index.php'</script>";
}
else{
//    if(isset($_SESSION['already'])){
//        $alert++;
//        unset($_SESSION['already']);
//    }
    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
    $profile_pic = $_SESSION['profile_pic'];
    $_SESSION['login']=2;
}
?>


<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="bootstrap.css">
        <link rel="icon" href="logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Tech Team Dashboard</title>
        <style>
            body{margin: 0; font-family: sans-serif;}
            a{
                text-decoration: none;
            }
            #alert{
                text-align: center;
                color: blue;
            }
            #logout{
                float: right;
                margin: auto;
            }
            nav{
                margin-bottom: 40px;
            }
            button{
                margin: 10px 20px;
            }
            .buttons{
                text-align: center;
                margin: 0 auto;
            }
            #profile{
                width: 40px;
                border-radius: 50%;
                margin-left: 5px;
            }
            #nav-menu{
                text-decoration: none;
                color: white;
            }

            .dropdown {
              position: relative;
              display: inline-block;
            }

            .dropdown-content {
              display: none;
              position: absolute;
              background-color: #f1f1f1;
              min-width: 100px; 
              max-width: 160px;
              box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
              z-index: 1;
            }

            .dropdown-content a {
                float: right;
                width: 100%;
                font-size: 12px;
              color: black;
              padding: 12px 16px;
              text-decoration: none;
              display: block;
            }

            .dropdown-content a:hover {background-color: #ddd;}

            .dropdown:hover .dropdown-content {display: block;}
            
        </style>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="">
                <img src="logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                Social Beat Tech Team Dashboard
            </a>
            <div class="dropdown">
            <div href="#" id="nav-menu" class="dropbtn">
                <?php echo $name; ?>
                <img src="<?php echo 'images\\'.$profile_pic ?>" id="profile">
                <div class="dropdown-content">
                <a href="profile.php">My Profile</a>
                <a href="logout.php">Log out</a>
              </div>
            </div>
            </div>
        </nav>
<!--
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item active">
                <a class="nav-link" id="ind-topic">Personal Session Topic<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Outing Suggestion</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">External Speaker Suggestion</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Today's Name</a>
              </li>
            </ul>
          </div>
        </nav>
-->
        
        <div class="buttons">
            <a href="ind-topic.php"><button class="btn btn-primary" type="button">Add topic for personal session</button></a>
            <a href="outing-sugg.php"><button class="btn btn-primary" type="button">Add suggestion for outing</button></a>
            <a href="speaker-sugg.php"><button class="btn btn-primary" type="button">Add suggestion for external speaker</button></a>
            <a href="name-gen.php"><button class="btn btn-primary" type="button">Find out today's name</button></a>
        </div>
        
        <script>
        
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
            
        </script>
    </body>
</html>