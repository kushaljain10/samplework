<?php
$alert=0;
session_start();
$_SESSION['todaysname'] = ' ';
if(!isset($_SESSION['name'])){
    $_SESSION['login']=1;
    echo "<script>window.location.href = 'index.php'</script>";
}
else{
    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
    $profile_pic = $_SESSION['profile_pic'];
    $_SESSION['login']=2;
}

if(array_key_exists('button', $_POST)){
    $alert = name_gen();
}
function name_gen(){
        $today = date("l").', '.date("M").' '.date("d").', '.date("Y");

        require_once('dbconfig.php');
    
        $sql = "SELECT distinct(name) from individual_topic";
        $result = $conn->query($sql);

        if($result->num_rows>0){
            $names = [];
            while($row = $result->fetch_assoc()){
                $names[] = $row['name'];
            }

        $totalnames = count($names);

        $todaysnumber = rand(0,($totalnames-1));

        $todaysname = $names[$todaysnumber];
            
        $_SESSION['todaysname'] = $todaysname;

//        echo "<script>alert('$todaysname')</script>";
            return 1;

        }
        else{
            echo "<script>alert('No names!');</script>";
        }
    
    }   

?>

<html>
<head>
    <link rel="stylesheet" href="bootstrap.css">
        <link rel="stylesheet" href="style.css">
    <link rel="icon" href="logo.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Name of the day!</title>
    </head>

    <body>

        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="main.php">
                <img src="logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                Social Beat Tech Team Dashboard
            </a>
            <div class="dropdown">
            <div href="#" id="nav-menu" class="dropbtn">
                <?php echo $name; ?>
                <img src="<?php echo 'images\\'.$profile_pic ?>" id="profile">
                <div class="dropdown-content">
                <a href="profile.php">My Profile</a>
                <a href="logout.php">Log out</a>
              </div>
            </div>
            </div>
        </nav>
    
        <main>
        
            <center>
                <form method="post">
                    <input  class="btn btn-info" id="generate" type="submit" name="button" value="Generate Name for Today" />
                    <div id="name"></div>
                </form>
            </center>    
            <?php
            
            if($alert==1){
                echo "<div id='namebox'>".$_SESSION['todaysname']."</div";
            }
            
            ?>
        </main>
        <script>
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
        </script>
    </body>
</html>
