<?php
session_start();
$alert=0;
if(!isset($_SESSION['name'])){
    $_SESSION['login']=1;
    echo "<script>window.location.href = 'index.php'</script>";
}
else{
    $ID = $_SESSION['id'];
    $email = $_SESSION['email'];
    $name = $_SESSION['name'];
    $pass = $_SESSION['password'];
    $profile_pic = $_SESSION['profile_pic'];
    $_SESSION['login']=2;
}

date_default_timezone_set("Asia/Calcutta");
$date = date('mdY his a', time());

require_once('dbconfig.php');

if(isset($_POST['submit'])){
    $name = $_POST['name'];
    $email2 = $_POST['email'];
    $pass = $_POST['password'];
    $pass2 = $_POST['password2'];
    
    $sql = "SELECT * FROM users where email = '$email2'";
    $result = $conn->query($sql);
    
    if (($result->num_rows > 0) && ($email!=$email2)) {
        $alert=1;
       }
    else if(is_uploaded_file($_FILES['userfile']['tmp_name'])){
        $uploaddir = "images/";
        $filename = $_FILES['userfile']['name'];
        $file_ext = pathinfo($filename, PATHINFO_EXTENSION);
        $imageFileType = strtolower(pathinfo("$uploaddir"."$filename",PATHINFO_EXTENSION));
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                $alert = 4;
            }
        else{
            $filename = $name.$date.".$file_ext";
            $uploadfile = $uploaddir.$filename;
            $sql = "UPDATE users SET profile_pic = '$filename' WHERE  email = '$email'";
            
            if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
               echo "Upload failed";
            }
            else{
                $conn->query($sql);
                $myFile = "images/"."$profile_pic";
                unlink($myFile);
                $profile_pic = $filename;
                $_SESSION['profile_pic'] = $profile_pic;
            }
            if($name!=$_SESSION['name']||$email!=$_SESSION['email']||$pass!=$_SESSION['password']){
            
                $sql = "UPDATE users SET name = '$name', email = '$email2', password = '$pass' WHERE  email = '$email'";

                if($conn->query($sql))
                {    
                    $_SESSION['name'] = $_POST['name'];
                    $_SESSION['email'] = $_POST['email'];
                    $_SESSION['password'] = $_POST['password'];
                    $email = $email2;
                    $alert=2;
                }
                else{
                    $alert=3;
                }
            }
               
        }
    }
    else if($pass!=$pass2){
        $pass = $_SESSION['password'];
        $alert = 5;
    }
    else{
        if($name!=$_SESSION['name']||$email!=$_SESSION['email']||$pass!=$_SESSION['password']){
            
            $sql = "UPDATE users SET name = '$name', email = '$email2', password = '$pass' WHERE  email = '$email'";
        
            if($conn->query($sql))
            {
                $_SESSION['name'] = $_POST['name'];
                $_SESSION['email'] = $_POST['email'];
                $_SESSION['password'] = $_POST['password'];
                $email = $email2;
                $alert=2;
            }
            else{
                $alert=3;
            }
        }
        
        }
    }

?>

<!DOCTYPE html>
<html>

    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="bootstrap.css">
        <link rel="stylesheet" href="style.css">
        <link rel="icon" href="logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>My Profile</title>
        <style>
            .form-group
            {
                position: relative;
                width: 80%;
                max-width: 400px;
                margin: 0 auto;
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        <nav class="navbar navbar-dark bg-dark">
            <a class="navbar-brand" href="main.php">
                <img src="logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
                Social Beat Tech Team Dashboard
            </a>
            <a href="logout.php"><button class="btn btn-light btn-sm" type="button" id="logout">Log out</button></a>
        </nav>
        <form method="post" enctype="multipart/form-data">
            <div class="form-group"><input class="btn btn-primary " type="submit" name="submit" value="Commit Changes" id="submit"/>
                <input class="btn btn-danger " type="button" name="cancel" value="Cancel" id="cancel"/>
                <input  class="btn btn-primary" type="button" value="Edit Profile" id="edit" />
            </div>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" id="name" value="<?php echo $name;?>" class="form-control" name="name" readonly>
              </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" value="<?php echo $email;?>" class="form-control" name="email" readonly>
              </div>
            <div class="form-group">
                <label for="password">Password</label><i id="pass-status" class="fa fa-eye" aria-hidden="true" onClick="viewPassword()"></i>
                <input type="password" id="password" value="<?php echo $pass;?>" class="form-control" name="password" readonly>
              </div>
            <div class="form-group" id="password2div">
                <label for="password2">Confirm Password</label>
                <input type="password" id="password2" value="<?php echo $pass;?>" class="form-control" name="password2">
              </div>
            <div class="form-group" id="profile-pic-box">
                <label for="profile-picture">Profile Picture</label>
                <?php echo '<img id="profile-picture" src="images\\'.$profile_pic.'?m=\' . filemtime(\'images\\'.$profile_pic.'\') . \'">';?>
              </div>
            <div class="form-group" id="profile-pic-box2">
                <label for="profile-picture">Profile Picture</label>
                  <div class="container">
                    <div class="picture-container">
                        <div class="picture">
                            <?php echo '<img src="images\\'.$profile_pic.'?m=\' . filemtime(\'images\\'.$profile_pic.'\') . \'" class="picture-src" id="wizardPicturePreview" title="">';?>
                            
                            <input type="file" name="userfile" id="userfile" class="">
                        </div>
                        <h6 class="">Choose Picture</h6>

                    </div>
                </div>
              </div>
        </form>
        <?php
            if(isset($alert)){
                switch($alert){
                    case 1: echo "<br><div id='alert'>Email ID already exists, try again.</div>";
                        break;
                    case 2: echo "<br><div id='alert' style='color: green;'>Profile updated successfully!</div>";
                        break;
                    case 3: echo "<br><div id='alert'>Error in updating profile.</div>";
                        break;
                    case 4: echo "<br><div id='alert'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
                        break;
                    case 5: echo "<br><div id='alert'>Passwords do not match, try again.</div>";
                        break;
                    default: break;
                }
            }
            ?>
        <script>
        
            document.getElementById('edit').addEventListener('click', function(){
                if(document.getElementById('alert'))
                    document.getElementById('alert').innerHTML = '';
                document.getElementById('name').readOnly = false;
                document.getElementById('email').readOnly = false;
                document.getElementById('password').readOnly = false;
                document.getElementById('password2div').style.display = 'block';
                document.getElementById('submit').style.display = 'inline-block';
                document.getElementById('cancel').style.display = 'inline-block';
                document.getElementById('edit').style.display = 'none';
                document.getElementById('profile-pic-box2').style.display = 'block';
                document.getElementById('profile-pic-box').style.display = 'none';

            });
            document.getElementById('cancel').addEventListener('click', function(){
                if(document.getElementById('alert'))
                    document.getElementById('alert').innerHTML = '';
                document.getElementById('name').readOnly = true;
                document.getElementById('email').readOnly = true;
                document.getElementById('password').readOnly = true;
                document.getElementById('password2div').style.display = 'none';
                
                document.getElementById('name').value = "<?php echo $name;?>";
                document.getElementById('email').value = "<?php echo $email;?>";
                document.getElementById('password').value = "<?php echo $pass;?>";
                document.getElementById('password2div').value = "<?php echo $pass;?>";
                
                document.getElementById('edit').style.display = 'inline-block';
                document.getElementById('submit').style.display = 'none';
                document.getElementById('cancel').style.display = 'none';
                document.getElementById('profile-pic-box2').style.display = 'none';
                document.getElementById('profile-pic-box').style.display = 'block';
            });
            document.getElementById('submit').addEventListener('click', function(){
                if(document.getElementById('alert'))
                    document.getElementById('alert').innerHTML = '';
                document.getElementById('name').readOnly = true;
                document.getElementById('email').readOnly = true;
                document.getElementById('password').readOnly = true;
                document.getElementById('password2div').style.display = 'none';
                document.getElementById('edit').style.display = 'inline-block';
                document.getElementById('submit').style.display = 'none';
                document.getElementById('cancel').style.display = 'none';
                document.getElementById('profile-pic-box2').style.display = 'none';
                document.getElementById('profile-pic-box').style.display = 'block';
            });
            
            $(document).ready(function(){
            // Prepare the preview for profile picture
                $("#userfile").change(function(){
                    readURL(this);
                });
            });
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
            
            function viewPassword()
            {
              var passwordInput = document.getElementById('password');
              var passStatus = document.getElementById('pass-status');

              if (passwordInput.type == 'password'){
                passwordInput.type='text';
                passStatus.className='fa fa-eye-slash';

              }
              else{
                passwordInput.type='password';
                passStatus.className='fa fa-eye';
              }
            }
        
        </script>
    </body>
</html>